package com.javachen.model;

import lombok.Data;

import javax.persistence.*;


@Entity
@Data
public class Item {
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Integer id;
  @Column
  private boolean checked;
  @Column
  private String description;
}


